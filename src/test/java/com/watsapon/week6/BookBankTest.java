package com.watsapon.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

public class BookBankTest {

    @Test
    public void shouldDepositSuc() {
        BookBank book = new BookBank("Name", 0);
        boolean result = book.deposit(100);
        assertEquals(true, result);
        assertEquals(100, book.getbalance(), 0.00001);
    }

    @Test
    public void shouldDepositNag() {
        BookBank book = new BookBank("Name", 0);
        boolean result = book.deposit(-100);
        assertEquals(false, result);
        assertEquals(0, book.getbalance(), 0.00001);
    }

    @Test
    public void shouldWithdrawSuc() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100);
        boolean result = book.withdraw(50);
        assertEquals(true, result);
        assertEquals(50, book.getbalance(), 0.00001);
    }

    @Test
    public void shouldWithdrawNeg() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100);
        boolean result = book.withdraw(-50);
        assertEquals(false, result);
        assertEquals(100, book.getbalance(), 0.00001);
    }

    @Test
    public void shouldWithdrawOver() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(50);
        boolean result = book.withdraw(100);
        assertEquals(false, result);
        assertEquals(50, book.getbalance(), 0.00001);
    }

    @Test
    public void shouldWithdraw100bal100() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100);
        boolean result = book.withdraw(100);
        assertEquals(true, result);
        assertEquals(0, book.getbalance(), 0.00001);
    }
}

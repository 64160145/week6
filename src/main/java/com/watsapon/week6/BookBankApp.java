package com.watsapon.week6;

import java.sql.SQLIntegrityConstraintViolationException;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank watsapon = new BookBank("Watsapon",100);
        watsapon.print();
        watsapon.deposit(50);
        watsapon.print();
        watsapon.withdraw(50);
        watsapon.print();

        BookBank prayood = new BookBank("Prayood",1);
        prayood.deposit(1000000);
        prayood.withdraw(10000000);
        prayood.print();

        BookBank praweet = new BookBank("Praweet",10);
        praweet.deposit(10000000);
        praweet.withdraw(1000000);
        praweet.print();
    }
}
